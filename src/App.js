import React from 'react';
import { Route, Switch} from "react-router-dom";
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import Navbar from "./component/Navbar";
import ProductList from "./component/ProductList";
import Default from "./component/Default";
import Cart from "./component/Cart";
import Product from "./component/Product";
import Details from "./component/Details";
import Modal from "./component/Modal";

function App() {
  return (
    <React.Fragment>
        <Navbar/>
        <Switch>
            <Route exact path="/" component={ProductList}/>
            <Route path="/details" component={Details}/>
            <Route path="/cart" component={Cart}/>
            <Route component={Default}/>
        </Switch>
        <Modal/>
    </React.Fragment>
  );
}

export default App;
